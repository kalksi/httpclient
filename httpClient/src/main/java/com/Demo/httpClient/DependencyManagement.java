package com.Demo.httpClient;

import services.Posts_Service;
import services.Posts_Service_Interface;
import services.UserConsole;
import services.UserConsole_Interface;
import services.Useres_Service;
import services.Useres_Service_Interface;

public class DependencyManagement {
	
	private static Posts_Service_Interface postsService;
	private static UserConsole_Interface userConsole;
	private static Useres_Service_Interface  useresService;
	
  
    
    
    public static Posts_Service_Interface getClientService() {
    	
    	if(postsService == null) {
    		
      		
    		return new Posts_Service();
    		
    	}else return postsService;
    	
    	
    }
    
    
    
    public static UserConsole_Interface getUserConsole() {
    	
    	if(userConsole == null) {
    		
    	    		
    		return new UserConsole();
    		
    	}else return userConsole;
    	
    	
    }
    
    
   public static Useres_Service_Interface getUseresService() {
    	
    	if(useresService == null) {
    		
    	    		
    		return new Useres_Service();
    		
    	}else return useresService;
    	
    	
    }
     
    
}
