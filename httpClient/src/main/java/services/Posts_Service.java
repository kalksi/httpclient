package services;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import modell.Model;

import java.util.List;

public class Posts_Service implements Posts_Service_Interface{

	private HttpClient client;

	public Posts_Service() {
		client = HttpClient.newHttpClient();
	}

	public List<Model> getAll(String uri) {

		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(uri)).build();

		HttpResponse<String> response = null;
		try {
			response = client.send(request, BodyHandlers.ofString());
		} catch (IOException | InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			return new ObjectMapper().readValue(response.body(), new TypeReference<List<Model>>() {
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public Model getByID(String uri) {

		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(uri)).build();

		HttpResponse<String> response = null;
		try {
			response = client.send(request, BodyHandlers.ofString());
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			return new ObjectMapper().readValue(response.body(), Model.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	List<Model> x;

	public void readAsynchronously(String uri, UserConsole obj,Runnable lambda) {

		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(uri)).build();

		client.sendAsync(request, BodyHandlers.ofString()).thenApply(response -> {
			System.out.println(response.statusCode());
			return response;
		}).thenApply(HttpResponse::body).thenAccept((r) -> {
			try {

				x = new ObjectMapper().readValue(r, new TypeReference<List<Model>>() {
				});
				// x.forEach(System.out::println);
				obj.UpdateUserConsol(x);
				lambda.run();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

	}

}
