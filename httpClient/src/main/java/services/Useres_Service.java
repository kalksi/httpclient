package services;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import modell.User;

public class Useres_Service implements Useres_Service_Interface{
	
	
	private HttpClient client;
	private List<User> defualtResult;
	
	public Useres_Service(){
		
		client = HttpClient.newHttpClient();
	}
	
	


	public List<User> getAll(String url) {
		
		HttpRequest request =HttpRequest.newBuilder().uri(URI.create(url)).build();
	
			HttpResponse<String> response = null;
			try {
				response = client.send(request, BodyHandlers.ofString());
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				defualtResult = new ObjectMapper().readValue(response.body(), new TypeReference<List<User>>() {
				});
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 return defualtResult ;
	}



	public User getById(String url) {
		// wie beim Posts abfrage
		return null;
	}

	public List<User> SortbyUser(String userName) {
	
		// wie beim Posts Sort
		return null;
	}

	public List<User> findByCompany(String CompanyName,String url) {
		
	/*
		// TODO Auto-generated method stub
		List<User> results = new ArrayList<User>();
		for(User user: defualtResult) {
			
			if (user.getCompany().getName().contains(CompanyName)) {
				results.add(user);
			};
			
		}
		
		
		oder..
		
		*/
	if(defualtResult == null) { getAll(url);}
		
		return  	defualtResult.stream().filter(user -> user.getCompany().getName().contains(CompanyName)).collect(Collectors.toList());
	}

}
