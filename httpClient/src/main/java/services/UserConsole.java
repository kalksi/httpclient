package services;


import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import com.Demo.httpClient.DependencyManagement;

import modell.Model;

public class UserConsole implements UserConsole_Interface{

	private Posts_Service_Interface postsService;
	private Useres_Service_Interface useresService;
	
	private String BaseUrl = "https://jsonplaceholder.typicode.com/";
	private Scanner scanner;
	String[] Optionen = { "Alle Posts Abfragen", "einen Posts Abfragen", "Post bei Title filteren", "filteren  bei Title Sort", "aSynchron alle Posts Abfragen", "Alle Users abfragen" ,"Users bei Frimen Filtern"};

	private  int testvar;



	public UserConsole() {
		postsService = DependencyManagement.getClientService();
		useresService =  DependencyManagement.getUseresService();
		scanner = new Scanner(System.in);
		
	
	}
	
	public void run()     {

		menu();

		while (true) {
			
			try {
				Thread.sleep(400);
				System.err.println("was wollen Sie machen?");
				System.out.print("  für Menu typen Sie menu ein.");
				Thread.sleep(400);
				System.out.print("..... oder! für schlissen typen Sie exit ein.\n");
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			var eingabe = scanner.nextLine();
			if (eingabe.equals("exit")) {
				System.err.println("byyyyyyyyyy!!");
				break;
			} else {

				switch (eingabe) {
				case "1":
					postsService.getAll(BaseUrl + "posts").forEach(System.out::println);
					break;
				case "2":
					System.err.println("Bitte ID eingeben:\n");
					System.out.println(postsService.getByID(BaseUrl + "post/" + getInput()));
					System.err.println("\n");
					break;

				case "3":
					System.err.println("Bitte Suchwort eingeben:\n");
					try {
						filter(getInput());
					} catch (Exception e) {
					  e.printStackTrace();
					}

					System.out.println("noch suchen? y/n\n");
					if (getInput().contentEquals("y")) {
						System.err.println("Bitte Suchwort eingeben:\n");
						try {
							filter(getInput());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						break;
					}
					break;
				case "4":
					sortByTitle(postsService.getAll(BaseUrl + "posts")).forEach(System.out::println);
					break;
				case "5":
					postsService.readAsynchronously(BaseUrl + "posts",  this,printItOut);
					System.out.println("from main" + testvar);

					break;
				case "6":
					useresService.getAll(BaseUrl+"users").forEach(System.out::println);
				break;
				case "7":
					System.err.println("Bitte Suchwort eingeben:\n");
					String filter= scanner.nextLine();
					System.err.println("\nFilter nach Name: "+filter);
					useresService.findByCompany(filter,BaseUrl+"users").forEach(System.out::println);
				break;
				case "menu":
					menu();
					break;
				default:
					menu();
					break;
				}

			}

		}

	}
	
	Runnable printItOut = () -> System.out.println("Ich habe hier keine sinnvolle Funktion! ich diene nur zum testen(Lambda als Parameter in Asyncfunktion überzugeben)"+ ++testvar);
	


	public  void UpdateUserConsol(List<Model> list) {

		list.forEach(System.out::println);
		//testvar hat keine Funktion!
		testvar = 5;
		System.out.println(testvar);
	}

	private void filter(String suchwort) throws Exception {

		List<Model> posts = postsService.getAll(BaseUrl + "posts");
		int ergebnisse = 0;
		for (Model model : posts) {
			if (model.getTitle().contains(suchwort)) {
				System.out.println(model);
				ergebnisse++;
			}
		}

		if (ergebnisse == 0) {
			System.out.println("leider keine Ergibnesse\n");

		} else {
			System.out.println(ergebnisse + " Ergibnesse wurde gefunden");
		}

	}

	private List<Model> sortByTitle(List<Model> list) {

		Collections.sort(list, new Comparator<Model>() {
			{
			}

			@Override
			public int compare(Model o1, Model o2) {
				// TODO Auto-generated method stub
				return o1.getTitle().compareTo(o2.getTitle());
			}
		});

		return list;

	}

	private String getInput() {

		return scanner.nextLine();
	}

	private void menu() {

		System.out.println("da sind Ihre Optionen;\n");

		for (int i = 0; i < Optionen.length; i++) {

			System.out.println(i + 1 + "- " + Optionen[i] + "\n");
		}
	}
}
