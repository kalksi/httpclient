package services;

import java.util.List;

import modell.Model;

public interface Posts_Service_Interface {
	
	public List<Model> getAll(String uri);
	public Model getByID(String uri);
	public void readAsynchronously(String uri, UserConsole obj,Runnable lambda);

}
